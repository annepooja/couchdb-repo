package com.jaxrs;

import org.ektorp.CouchDbInstance;
import org.ektorp.http.StdHttpClient;
import org.ektorp.http.HttpClient;
import org.ektorp.impl.StdCouchDbInstance;

import java.net.MalformedURLException;

public class DBConnector {
    private DBConnector(){}

    private static CouchDbInstance dbInstance;

    public static CouchDbInstance getDbInstance(){
        try{
            HttpClient httpClient = new StdHttpClient.Builder().url("localhost:5984")
                .build();
            dbInstance = new StdCouchDbInstance(httpClient);
        }catch (MalformedURLException ex){

        }
        return dbInstance;
    }
}
