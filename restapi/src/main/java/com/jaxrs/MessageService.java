package com.jaxrs;

import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MessageService {

    private CouchDbInstance dbInstance = DBConnector.getDbInstance();
    private CouchDbConnector dbConnector = null;
    private Map<Long, Message> messages = DatabaseClass.getMessages();
    public MessageService()
    {
        messages.put(10000l, new Message(10, "Welcome"));
        messages.put(20000l, new Message(20, "JAX-RS"));

        dbConnector=dbInstance.createConnector("message",true);
        dbConnector.createDatabaseIfNotExists();
    }

    public List<Message> getallMessages()
    {
        Message m1 = new Message(1, "Welcome");
        Message m2 = new Message(2, "JAX_RS");
        List<Message> list = new ArrayList<>();
        list.add(m1);
        list.add(m2);
        return list;
    }

    public Message addMessage(Message message)
    {
        message.setId(messages.size()+1);
        messages.put(message.getId(), message);
        return message;
    }

    public Message updateMessage(Message message)
    {
        if(message.getId() <= 0)
        {
            return null;
        }
        messages.put(message.getId(), message);
        return message;
    }

    public Message removeMessage(long id)
    {
        return messages.remove(id);
    }
}
