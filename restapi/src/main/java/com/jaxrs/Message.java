package com.jaxrs;

import java.util.Date;

public class Message {

    private long id;
    private String message;
    private Date created;

    public Message()
    {

    }

    public Message(long id, String message) {
        super();
        this.id = id;
        this.message = message;
        this.created = new Date();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
