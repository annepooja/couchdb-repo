package com.jaxrs;

//import sun.java2d.cmm.Profile;

import com.sun.tools.javac.jvm.Profile;

import java.util.HashMap;
import java.util.Map;

public class DatabaseClass {

    private static Map<Long, Message> messages = new HashMap<>();
    private static Map<Long, Profile> profiles = new HashMap<>();

    public static Map<Long, Message> getMessages()
    {
        return messages;
    }

    public static Map<Long, Profile> getProfiles()
    {
        return profiles;
    }
}
